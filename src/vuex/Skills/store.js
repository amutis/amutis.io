const state = {
  all_skills: [
    {
      name: 'Vue.js',
      logo_url: 'https://cn.vuejs.org/images/logo.png'
    },
    {
      name: 'Html 5',
      logo_url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/HTML5_Badge.svg/600px-HTML5_Badge.svg.png'
    },
    {
      name: 'CSS3',
      logo_url: 'http://pngmonkey.com/wp-content/uploads/00173.jpg'
    },
    {
      name: 'Javascript',
      logo_url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/480px-Unofficial_JavaScript_logo_2.svg.png'
    },
    {
      name: 'CSS Grid',
      logo_url: 'https://i.ytimg.com/vi/-fDqBEjfzGo/maxresdefault.jpg'
    },
    {
      name: 'UIKit',
      logo_url: 'https://cdn.worldvectorlogo.com/logos/uikit.svg'
    },
    {
      name: 'Bootstrap',
      logo_url: 'https://getbootstrap.com/assets/img/bootstrap-stack.png'
    },
    {
      name: 'Vuetify',
      logo_url: 'https://seeklogo.com/images/V/vuetify-logo-3BCF73C928-seeklogo.com.png'
    },
    {
      name: 'Wordpress',
      logo_url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Wordpress-Logo.svg/500px-Wordpress-Logo.svg.png'
    },
    {
      name: 'Firebase',
      logo_url: 'https://seeklogo.com/images/F/firebase-logo-402F407EE0-seeklogo.com.png'
    },
    {
      name: 'Pusher',
      logo_url: 'http://www.stickpng.com/assets/images/5848153ecef1014c0b5e4969.png'
    },
    {
      name: 'Tweenmax',
      logo_url: 'https://greensock.com/forums/uploads/packages-0701062001407339578.png'
    },
    {
      name: 'Gitlab',
      logo_url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/2000px-GitLab_Logo.svg.png'
    }
  ],
  skills: [],
  skill: [],
  deleted_skills: []
}

export default {
  state
}
