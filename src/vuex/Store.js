import Vue from 'vue'
import Vuex from 'vuex'

import ProjectStore from './Projects/store'
import SkillStore from './Skills/store'

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.config.debug = true

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    ProjectStore,
    SkillStore
  },
  strict: debug
})
