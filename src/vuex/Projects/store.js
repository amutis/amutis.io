const state = {
  all_projects: [
    {
      name: 'Lykaah',
      main_image: '/static/Projects/lykaah/LykaahLandingPage.png',
      live_url: '',
      demo_url: 'http://lykaah.com',
      platforms: [8, 1, 2],
      other_images: [
        {
          image: '/static/Projects/lykaah/LykaahLandingPage.png',
          caption: 'Landing Page'
        },
        {
          image: '/static/Projects/lykaah/LykaahSearch.png',
          caption: 'Search through tags, categories and articles'
        },
        {
          image: '/static/Projects/lykaah/LykaahArticleWithNoSideBar.png',
          caption: 'A single article.'
        },
        {
          image: '/static/Projects/lykaah/ArticleWithSideBar.png',
          caption: 'A single article with a sidebar'
        },
        {
          image: '/static/Projects/lykaah/LykaahVottingSharingAndAuthor.png',
          caption: 'Commenting, votting and sharing of an article with the author details.'
        }
      ],
      details: 'A wordpress Blog with articles for beauty, fashion and lifestyle'
    },
    {
      name: 'Olpejeta Conservancy',
      main_image: '/static/Projects/opc/MainPage.png',
      live_url: '',
      demo_url: 'http://public.bima-club.com',
      platforms: [0, 1, 2, 3, 12],
      other_images: [
        {
          image: '/static/Projects/opc/MainPage.png',
          caption: 'Landing Page'
        },
        {
          image: '/static/Projects/opc/Catalog.png',
          caption: 'The Conservancy Catalog.'
        },
        {
          image: '/static/Projects/opc/DatePicker.png',
          caption: 'Picking of dates during booking.'
        },
        {
          image: '/static/Projects/opc/GuestPicker.png',
          caption: 'Selecting the number of guests arriving.'
        },
        {
          image: '/static/Projects/opc/VehiclePicker.png',
          caption: 'Selecting the number of vehicles.'
        },
        {
          image: '/static/Projects/opc/AccommodationSelection.png',
          caption: 'Choosing one\'s accommodation.'
        },
        {
          image: '/static/Projects/opc/ActivitySelection.png',
          caption: 'Choosing activities one will participate in.'
        },
        {
          image: '/static/Projects/opc/Checkout.png',
          caption: 'Filling details and checking out.'
        }
      ],
      details: 'A Kenyan Wildlife Conservancy. Client wanted booking on their sie to move away from checkfront.'
    }
  ],
  projects: [],
  project: [],
  deleted_projects: []
}

export default {
  state
}
