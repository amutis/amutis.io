import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import IndexPage from '@/components/Pages/IndexPage'
import Projects from '@/components/Pages/Projects'
import PlayHouse from '@/components/Pages/PlayHouse'
import Samples from '@/components/Pages/Samples'
import Animations from '@/components/Pages/Animations'
import SingleProject from '@/components/Pages/SingleProject'
import HireMe from '@/components/Pages/HireMe'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Main,
      children: [
        {
          path: '',
          name: 'IndexPage',
          component: IndexPage
        },
        {
          path: 'projects',
          name: 'Projects',
          component: Projects
        },
        {
          path: 'play-house',
          name: 'PlayHouse',
          component: PlayHouse
        },
        {
          path: 'samples',
          name: 'Samples',
          component: Samples
        },
        {
          path: 'animations',
          name: 'Animations',
          component: Animations
        },
        {
          path: 'project/:project_id',
          name: 'SingleProject',
          component: SingleProject
        },
        {
          path: 'hire-me',
          name: 'HireMe',
          component: HireMe
        }
      ]
    }
  ],
  mode: 'history'
})
